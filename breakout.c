//
// breakout.c
//
// Computer Science 50
// Problem Set 4
//

// standard libraries
#define _XOPEN_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

// Stanford Portable Library
#include "gevents.h"
#include "gobjects.h"
#include "gwindow.h"

// height and width of game's window in pixels
#define HEIGHT 600
#define WIDTH 400

// number of rows of bricks
#define ROWS 5

// number of columns of bricks
#define COLS 10

// radius of ball in pixels
#define RADIUS 10

// lives
#define LIVES 3

// prototypes
void initBricks(GWindow window);
GOval initBall(GWindow window);
GRect initPaddle(GWindow window);
GLabel initScoreboard(GWindow window);
void updateScoreboard(GWindow window, GLabel label, int points);
GObject detectCollision(GWindow window, GOval ball);

int main(void)
{
    // seed pseudorandom number generator
    srand48(time(NULL));

    // instantiate window
    GWindow window = newGWindow(WIDTH, HEIGHT);

    // instantiate bricks
    initBricks(window);

    // instantiate ball, centered in middle of window
    GOval ball = initBall(window);

    // instantiate paddle, centered at bottom of window
    GRect paddle = initPaddle(window);

    // instantiate scoreboard, centered in middle of window, just above ball
    GLabel label = initScoreboard(window);

    // number of bricks initially
    int bricks = COLS * ROWS;

    // number of lives initially
    int lives = LIVES;

    // number of points initially
    int points = 0;

    double velocity_x = drand48() + 1;
    double velocity_y = drand48() + 1;

    waitForClick();

    // keep playing until game over
    while (lives > 0 && bricks > 0)
    {
      GEvent event = getNextEvent(MOUSE_EVENT);

      if (event != NULL)
	{
	  if (getEventType(event) == MOUSE_MOVED)
	    {
	      double x = getX(event) - (getWidth(paddle) / 2);
	      setLocation(paddle, x, HEIGHT - 50);
	      if (getX(paddle) <= 0)
		setLocation(paddle, 0, HEIGHT - 50);
	      else if (getX(paddle) + getWidth(paddle) >= getWidth(window))
		setLocation(paddle, getWidth(window) - getWidth(paddle), HEIGHT - 50);
		  
	    }
	}
      
      GObject object = detectCollision(window, ball);

      if (object != NULL)
	{
	  if (object == paddle)
	    {
	      velocity_y = -velocity_y;
	    }

	  else if (strcmp(getType(object), "GRect") == 0)
	    {
	      removeGWindow(window, object);
	      velocity_y = -velocity_y;
	      if (velocity_y < 4.2)
		{
		  velocity_y += 0.21;
		  velocity_x += 0.21;
		}
	      points++;
	    }
	}

      if (getX(ball) + getWidth(ball) >= getWidth(window))
	{
	  velocity_x = -velocity_x;
	}
      else if (getX(ball) <= 0)
	{
	  velocity_x = -velocity_x;
	}
      if (getY(ball) + getHeight(ball) >= getHeight(window))
	{
	  velocity_y = -velocity_y;
	}
      else if (getY(ball) <= 0)
	{
	  velocity_y = -velocity_y;
	}

      move(ball, velocity_x, velocity_y);
      pause(10);
      updateScoreboard(window, label, points);
    }

    // wait for click before exiting
    waitForClick();

    // game over
    closeGWindow(window);
    return 0;
}

/**
 * Initializes window with a grid of bricks.
 */
void initBricks(GWindow window)
{
  int j;
  int brick_x;
  int brick_y = 35;
  GRect brick;
  char colors[6][7] = {
    "RED",
    "ORANGE",
    "YELLOW",
    "GREEN",
    "CYAN"
  };
  
  for (int i = 0; i < ROWS; i++) 
    {
      brick_x = 2;
      for (j = 0; j < COLS; j++)
	{
	  brick = newGRect(brick_x, brick_y, 35, 10);
	  setColor(brick, colors[i]);
	  setFilled(brick, true);
	  add(window, brick);
	  brick_x += 40;
	}
      brick_y += 15;
    }
}

/**
 * Instantiates ball in center of window.  Returns ball.
 */
GOval initBall(GWindow window)
{
  double x = (getWidth(window) - 20) / 2;
  double y = (getHeight(window) - 20) / 2;
 
  GOval ball = newGOval(x, y - RADIUS, RADIUS * 2, RADIUS * 2);
  setColor(ball, "BLACK");
  setFilled(ball, true);
  add(window, ball);
  return ball;
}

/**
 * Instantiates paddle in bottom-middle of window.
 */
GRect initPaddle(GWindow window)
{
  double x = (getWidth(window) - 75) / 2;
  double y = HEIGHT - 50;
  
  GRect rect = newGRect(x, y, 75, 12);
  setColor(rect, "BLACK");
  setFilled(rect, true);
  add(window, rect);
  return rect;
}

/**
 * Instantiates, configures, and returns label for scoreboard.
 */
GLabel initScoreboard(GWindow window)
{
  GLabel label = newGLabel("0");
  setFont(label, "SansSerif-32");
  setColor(label, "GRAY");
  double x = (getWidth(window) - getWidth(label)) / 2;
  double y = (getHeight(window) - getHeight(label)) / 2;
  setLocation(label, x, y);
  add(window, label);
  return label;
}

/**
 * Updates scoreboard's label, keeping it centered in window.
 */
void updateScoreboard(GWindow window, GLabel label, int points)
{
    // update label
    char s[12];
    sprintf(s, "%i", points);
    setLabel(label, s);

    // center label in window
    double x = (getWidth(window) - getWidth(label)) / 2;
    double y = (getHeight(window) - getHeight(label)) / 2;
    setLocation(label, x, y);
}

/**
 * Detects whether ball has collided with some object in window
 * by checking the four corners of its bounding box (which are
 * outside the ball's GOval, and so the ball can't collide with
 * itself).  Returns object if so, else NULL.
 */
GObject detectCollision(GWindow window, GOval ball)
{
    // ball's location
    double x = getX(ball);
    double y = getY(ball);

    // for checking for collisions
    GObject object;

    // check for collision at ball's top-left corner
    object = getGObjectAt(window, x, y);
    if (object != NULL)
    {
        return object;
    }

    // check for collision at ball's top-right corner
    object = getGObjectAt(window, x + 2 * RADIUS, y);
    if (object != NULL)
    {
        return object;
    }

    // check for collision at ball's bottom-left corner
    object = getGObjectAt(window, x, y + 2 * RADIUS);
    if (object != NULL)
    {
        return object;
    }

    // check for collision at ball's bottom-right corner
    object = getGObjectAt(window, x + 2 * RADIUS, y + 2 * RADIUS);
    if (object != NULL)
    {
        return object;
    }

    // no collision
    return NULL;
}
